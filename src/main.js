// External modules
const config = require('config');
// Internal modules
const logger = require('./modules/logger');
const server = require('./server');

// Entry point of the server
const expressInstance = server.start(database_client => {
    if (database_client === null) {
        logger.error('Failed to initialize server');
        return;
    }

    // Warnings
    if (config.get('mail.enable') === true) {
        logger.warn_undefined(process.env.MAIL_USERNAME, 'MAIL_USERNAME');
        logger.warn_undefined(process.env.MAIL_PASSWORD, 'MAIL_PASSWORD');
    }

    // General console information
    [
        `Server is${process.env.NODE_ENV !== 'production' ? ' NOT' : ''} in production mode`,
        `Mailing is ${config.get('mail.enable') === true ? 'enabled' : 'disabled'}`,
        `Upload is ${config.get('pages.upload.enable') === true ? 'enabled' : 'disabled'}`,
        `Uploaded pictures will be stored in ${config.get('pages.storage.path')}/`,
        `Node server listening on port ${config.get('server.port')} using ${database_client}`
    ].forEach(str => {
        logger.log(str);
    });
});

// Server halt handling
const exitHandler = (_, exitCode) => {
    if (exitCode !== 0)
        logger.error(`Node server stopping (${exitCode})`);
    else
        logger.log(`Node server stopping (${exitCode})`);
    server.cleanup(expressInstance);
    process.exit(exitCode);
}

process.on('exit',              exitHandler.bind(null, {}));
process.on('SIGINT',            exitHandler.bind(null, {}));
process.on('SIGTERM',           exitHandler.bind(null, {}));
process.on('SIGILL',            exitHandler.bind(null, {}));
process.on('SIGQUIT',           exitHandler.bind(null, {}));
process.on('SIGTSTP',           exitHandler.bind(null, {}));
process.on('SIGUSR1',           exitHandler.bind(null, {}));
process.on('SIGUSR2',           exitHandler.bind(null, {}));
process.on('uncaughtException', exitHandler.bind(null, {}));