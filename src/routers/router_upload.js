// External modules
const config = require('config');
const express = require('express');
// Internal modules
const page_data = require('../middleware/page_data');
const upload = require('../controllers/upload');

module.exports = express.Router()

// Page de téléversement
.get('/', page_data, (req, res) => {
    upload.upload_page({ req: req, res: res });
})

// Page de gestion de l'erreur 413 (Entity too large)
.get('/413', page_data, (req, res) => {
    upload.upload_entity_too_large({ req: req, res: res });
})

// Téléversement d'un fichier
.post('/', page_data, (req, res) => {
    upload.upload({ req: req, res: res }, config.get('pages.storage.path') + '/', req.files, req.ip, req.body.private !== undefined, req.body.tags, req.body.alt, req.body.temp);
});