// External modules
const express = require('express');
// Internal modules
const contact = require('../controllers/contact');
const page_data = require('../middleware/page_data');

module.exports = express.Router()

// Page de contact
.get('/', page_data, (_, res) => {
    contact.page(res);
})

.post('/', page_data, (req, res) => {
    contact.message({req, res}, req.body.message, req.body.email, req.ip);
});