// External modules
const express = require('express');
// Internal modules
const page_data = require('../middleware/page_data');

// Affichage de la page de maintenance, indépendemment de l'URI demandée
module.exports = express.Router().all('/*', page_data, (req, res) => {
    res.render('maintenance.ejs', { ...req.common_data });
});