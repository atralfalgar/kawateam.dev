// External modules
const express = require('express');
// Internal modules
const page_data = require('../middleware/page_data');
const view = require('../controllers/view');

module.exports = express.Router()

// Main view page
.get('/', page_data, (req, res) => {
    view.page({ req: req, res: res });
})
.get('/:upload_path', page_data, (req, res) => {
    view.page({ req: req, res: res }, req.params.upload_path);
});