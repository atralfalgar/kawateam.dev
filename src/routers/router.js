// External modules
const config = require('config');
const express = require('express');
// Internal modules
const accueil = require('../controllers/accueil');
const page_data = require('../middleware/page_data');
const page_404 = require('../controllers/404');

const router = express.Router();

// Internal routers

if (config.get('pages.contact.enable') === true)
    router.use(config.get('server.base_uri') + config.get('pages.contact.uri'), require('./router_contact'));
if (config.get('pages.upload.enable') === true)
    router.use(config.get('server.base_uri') + config.get('pages.upload.base_uri'), require('./router_upload'));
if (config.get('pages.storage.enable') === true) {
    router.use(config.get('server.base_uri') + config.get('pages.storage.base_uri'), require('./router_storage'));
    router.use(config.get('server.base_uri') + config.get('pages.view.base_uri'), require('./router_view'));
}

// Generic routes

router.get(config.get('server.base_uri') + '/', page_data, (req, res) => {
    accueil.accueil({ req: req, res: res });
})

.get(config.get('server.base_uri') + '/404', page_data, (req, res) => {
    page_404.page_404({ req: req, res: res });
})

.all(config.get('server.base_uri') + '/robots.txt', (_, res) => {
    res.type('text/plain');
    res.send("User-agent: *\nDisallow: /");
})

.all(config.get('server.base_uri') + '/teapot', (_, res) => {
    // I am a teapot
    res.writeHead(418);
    res.end();
});

module.exports = router;