// External modules
const config = require('config');
const express = require('express');
// Internal modules
const page_data = require('../middleware/page_data');
const storage = require('../controllers/storage');

const routerRoot = `${config.get('server.base_uri')}${config.get('pages.storage.base_uri')}/`;

module.exports = express.Router({ mergeParams: true })

// Page, no search
.get('/page/:page', page_data, (req, res) => {
    storage.get_storage(res, req.params.page, '');
})

// Page and search
.get('/:query/page/:page', page_data, (req, res) => {
    storage.get_storage(res, req.params.page, req.params.query);
})

// Search, no page
.get('/:query', page_data, (req, res) => {
    storage.get_storage(res, null, req.params.query);
})

// Storage search
.post('/', (req, res) => {
    if (req.body.search == null)
        res.redirect(routerRoot);
    else
        res.redirect(routerRoot + req.body.search);
})

// Report
.post('/report', (req, res) => {
    storage.report(res, Number(req.body.report[0]), req.ip);
})

// Main page storage
.get('/', page_data, (_, res) => {
    storage.get_storage(res, null, '');
});