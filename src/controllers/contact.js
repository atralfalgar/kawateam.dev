// External modules
const config = require('config');
// Internal modules
const db_contact = require('../database/db_contact');
const header_footer_builder = require('../modules/header_footer_builder');
const mailing = require('../modules/mailing');

/**
 * @brief Affiche la page de contact
 * @param {object} express Express res
*/
module.exports.page = express => {
    express.render('contact.ejs', { success: true, error: null, title: header_footer_builder.title('Contact'), ...express.req.common_data });
}

/**
 * @brief Envoi un message de contact
 * @param {object} res Express res
 * @param {string} message Message du contact
 * @param {string} email Email du contact (facultatif)
 * @param {string} ip IP du contact
*/
module.exports.message = (express, message, email, ip) => {
    if (!message || !ip)
        return express.res.render('contact.ejs', { success: false, error: err, title: header_footer_builder.title('Contact'), ...express.req.common_data });
    
    if (!email)
        email = '';

    db_contact.add({
        message: message,
        email: email,
        ip: ip
    })
    .then(_ => {
        mailing.send_email_admins(
            'Prise de contact',
            `Quelqu'un (${email} ${ip}) a envoyé un message aux administrateurs : \n${message}`)
        .then(_ => {})
        .catch(_ => {});
        express.res.redirect(`${config.get('server.base_uri')}/`);
    })
    .catch(err => {
        express.res.render('contact.ejs', { success: false, error: err, title: header_footer_builder.title('Contact'), ...express.req.common_data });
    });
}