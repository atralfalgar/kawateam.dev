// External modules
const config = require('config');
const mime = require('mime-types');
// Internal modules
const chain_promises = require('../modules/chain_promises');
const db_upload = require('../database/db_upload');
const db_reports = require('../database/db_reports');
const header_footer_builder = require('../modules/header_footer_builder');
const logger = require('../modules/logger');
const mailing = require('../modules/mailing');
const pagination = require('../modules/pagination');

const limitResult = config.get('pages.storage.pagination_size');

const thumbnailsPathProcess = data => {
    data.forEach(storage_entry => {
        storage_entry.thumbnail = this.thumbnail_path(storage_entry.path);
    });
    return data;
}

/**
 * @brief Génère l'url vers la vignette en prenant en compte le type original
 * @param {string} path L'url vers la vignette telle qu'elle est stockée en base de donnée
 * @returns L'url de la vignette à utiliser
 */
module.exports.thumbnail_path = path => {
    const base_uri_path = `${config.get('server.base_uri')}${config.get('pages.storage.static_uri')}thumbnails/${path}`;
    const mime_lookup = mime.lookup(path);
    
    if (!mime_lookup)
        return base_uri_path;

    if (mime_lookup.includes('video'))
        return base_uri_path + '.png';
    else if (mime_lookup.includes('audio'))
        return null;
    else
        return base_uri_path;
}

/**
 * @brief Affiche la page de stockage
 * @param {object} express Express res
 * @param {integer} page Numéro de page courrante (optionel)
 * @param {string} search Texte de recherche (optionel)
 */
module.exports.get_storage = (express, page, search) => {
    if (page === null)
        page = 1;
    
    const promises = [
        { title: 'tags', promise: db_upload.get_public_tags, args: null },
        { title: 'itemCount', promise: db_upload.request_count, args: search }
    ];

    chain_promises.chain(promises)
    .then(data => {
        const search_not_null = search == null ? '' : search;
        db_upload.storage((page - 1) * limitResult, limitResult, config.get('pages.storage.sort'), search_not_null)
        .then(storage => {
            if (page <= 0 || page > pagination.pageCount(data.itemCount.itemsAvailable, limitResult)) {
                express.redirect(config.get('server.base_uri') + config.get('pages.storage.base_uri') + '/page/1');
                return;
            }

            // On sépare les téléversements multi-taggés
            let tags = [];
            data.tags.forEach(tag => tag.tags.split(' ').forEach(splitedTag => {
                if (tags.findIndex(e => e === splitedTag) === -1)
                    tags.push(splitedTag);
            }));
    
            const storageBaseUrl = config.get('server.base_uri') + config.get('pages.storage.base_uri') + (search_not_null !== '' ? ('/' + search) : '');
            express.render('storage.ejs',
            {
                error: null,
                pagination: pagination.pagination(storageBaseUrl, parseInt(page), data.itemCount.itemsAvailable, limitResult),
                storage: thumbnailsPathProcess(storage),
                tags: tags,
                title: header_footer_builder.title('Stockage'),
                ...express.req.common_data
            });
        }); 
    })
    .catch(err => {
        logger.error(err);
        express.render('storage.ejs', {
            error: err, storage: [],
            pagination: pagination.pagination_error,
            title: header_footer_builder.title('Stockage'),
            dev: header_footer_builder.footer(),
            config: config });
    });
}

/**
 * @brief Signale un téléversement. Affiche ensuite de nouveau le stockage
 * @param {object} express Express res
 * @param {integer} id Id du téléversement signalé
 * @param {string} ip Adresse IP origine du signalement
 */
module.exports.report = (express, id, ip) => {
    const redirect_success = `${config.get('server.base_uri')}${config.get('pages.storage.base_uri')}`;

    db_reports.report({
        picture_id: id,
        reporter_ip: ip
    })
    .then(_ => {
        db_upload.get_by_id(id)
        .then(upload => {
            mailing.send_email_admins('Nouvelle image signalée',
                `Un téléversement (https://${config.get('server.name')}${config.get('server.base_uri')}
                ${config.get('pages.view.base_uri')}/${upload.path} ;
                id : ${id}) a été signalé par ${ip.replace(/\./g, '. ')}`)
            .then(_ => {
                express.redirect(redirect_success);
            });
        })
        .catch(_ => {
            mailing.send_email_admins('Nouvelle image signalée',
                `Un téléversement (id : ${id}) a été signalé par ${ip}`)
            .then(_ => {
                express.redirect(redirect_success);
            });
        });
    })
    .catch(err => {
        express.redirect(`${redirect_success}?error=${encodeURIComponent(err)}`);
    });
}