// External modules
const config = require('config');
const mime = require('mime-types');
// Internal modules
const datetime = require('../modules/datetime');
const db_upload = require('../database/db_upload');
const file_system = require('../modules/file_system');
const header_footer_builder = require('../modules/header_footer_builder');
const logger = require('../modules/logger');
const storage = require('./storage');

module.exports.page = (express, upload_path) => {
    if (!upload_path) {
        express.res.redirect('/storage');
        return;
    }

    const file_size = file_system.humain_display_size(file_system.get_file_size(`${config.get('pages.storage.path')}/${upload_path}`));

    db_upload.get_by_path(upload_path)
    .then(upload_data => {
        express.res.render('view.ejs',
        {
            data: {
                alt: upload_data.alt == '' ? upload_path : upload_data.alt,
                audio_thumbnail: mime.contentType(upload_path).includes('audio'),
                date: datetime.display_formatting(upload_data.date_upload),
                index: upload_data.id,
                path: upload_path,
                size: file_size === -1 ? '' : file_size,
                tags: upload_data.tags,
                thumbnail: storage.thumbnail_path(upload_path),
                type: mime.contentType(upload_path)
            },
            error: null,
            title: header_footer_builder.title(config.get('pages.view.title')),
            ...express.req.common_data
        });
    })
    .catch(err => {
        logger.error(err);
        express.res.redirect('/storage');
    });
}