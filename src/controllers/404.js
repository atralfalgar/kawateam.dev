// Internal modules
const header_footer_builder = require('../modules/header_footer_builder');

/**
 * @brief Affiche une page d'erreur correspondant au code erreur 404
 * @param {object} express Instance express (res, req)
 */
module.exports.page_404 = express => {
    express.res.status(404);
    express.res.render('404.ejs', { title: header_footer_builder.title('404'), ...express.req.common_data });
}