// Node modules
const path = require('path');
// External modules
const config = require('config');
const mime = require('mime-types');
const { v4: uuidv4 } = require('uuid');
// Internal modules
const datetime = require('../modules/datetime');
const db_upload = require('../database/db_upload');
const file_system = require('../modules/file_system');
const header_footer_builder = require('../modules/header_footer_builder');
const logger = require('../modules/logger');
const thumbnail = require('../modules/thumbnail');

const base_uri = config.get('server.base_uri');
const view_uri = config.get('pages.view.base_uri');

// Tableau avec les mime types acceptés pour le téléversement
const ACCEPTED_MIME_TYPES = config.get('pages.upload.accepted_mime_types');

// Affiche la page de téléversement
const display_upload_page = (express, success, error = null) => {
    express.res.render('upload.ejs', {
        accepted_mimes: file_system.accepted_mimes(ACCEPTED_MIME_TYPES),
        accepted_types: file_system.accepted_types(ACCEPTED_MIME_TYPES),
        error: error,
        success: success,
        title: header_footer_builder.title('Téléversement'),
        ...express.req.common_data
    });
}

/**
 * @brief La taille maximale de téléversement en octets
 */
module.exports.MAX_UPLOAD_SIZE =
    config.get('pages.upload.max_upload_size_mo') * 1048576 + 1;

/**
 * @brief Téléverse un fichier, contrôle sa validité et redirige vers sa vue
 * @param {object} express Instance express ({req, res}) 
 * @param {string} destDir Chemin vers le dossier des fichiers téléversés
 * @param {object} files Objet des fichiers reçus par express
 * @param {string} ip Adresse IP à l'origine du téléversement
 * @param {boolean} private Indique si c'est une téléversement privé ou non
 * @param {string} tag Tag renseigné lors du téléversement
 * @param {string} alt Texte alternatif renseigné lors du téléversement
 * @param {string} temp Nombre d'heures de conservation. 0 si permanent
 */
module.exports.upload =
        async (express, destDir, file, ip, private, tag, alt, temp) => {
    let uploadPath = null;
    try {
        // Cas d'erreurs
        if (file == null || file.picFile === undefined)
            throw 'Pas de fichier';
        if (file.picFile.size === this.MAX_UPLOAD_SIZE)
            throw 'Le fichier dépasse la taille maximale';
        const mimetype = mime.contentType(file.picFile.name);
        if (!ACCEPTED_MIME_TYPES.includes(mimetype))
            throw 'Type de fichier invalide';
        
        // On limite le nombre de téléversement pour une même ip.
        // Si la valeur de la conf est 0, on ne limite pas
        const max_upload = config.get('pages.upload.max_upload_ip');
        const count_uploads_ip = await db_upload.get_upload_count_last_hour_for_ip(ip);
        if (count_uploads_ip.total > max_upload && max_upload !== 0)
            throw `Maximum de téléversements pour ${ip} atteint`;
  
        // Téléversement, génération de la vignette et ajout dans la bdd
        const pathToUpload = `${uuidv4()}${path.extname(file.picFile.name)}`;
        await file.picFile.mv(`${destDir}${pathToUpload}`);
        uploadPath = pathToUpload;
        //resp = await downloadFile(file.picFile, destDir, mimetype);
        const res = await thumbnail.generate(uploadPath, destDir, mimetype);
        await db_upload.add({
            path: uploadPath,
            uploader: ip,
            private: (private ? '1' : '0'),
            tags: tag.replace(/%20/gium, ','),
            alt: alt,
            temp: (temp ? Number(temp) : 0)
        });

        // Rediraction vers sa page vue
        express.res.redirect(`${base_uri}${view_uri}/${res.path}`);
    }
    catch (e) {
        let error_msg = e;
        if (typeof e === 'object')
            error_msg = e.message;

        logger.error(error_msg, 'Upload');
        if (uploadPath) {
            file_system.delete_file(`${destDir}${uploadPath}`);
            file_system.delete_file(`${destDir}thumbnails/${uploadPath}`);
        }
        display_upload_page(express, false, `Téléversement échoué : ${error_msg}`);
    }
}

/**
 * @brief Affiche la page de téléversement
 * @param {object} express Instance express (req, res)
 */
module.exports.upload_page = express => {
    display_upload_page(express, true);
}

/**
 * @brief Affiche la page de téléversement avec une erreur 413
 * @param {object} express Instance express (req, res)
 */
module.exports.upload_entity_too_large = express => {
    display_upload_page(express,
        false,
        'Le fichier dépasse la taille maximale');
}

/**
 * @brief Supprime les téléversements temporaires expirés
 */
module.exports.remove_expired_temp_uploads = _ => {
    db_upload.get_temp_uploads()
    .then(temp_uploads => {
        temp_uploads.forEach(temp_upload => {
            const date_expire = datetime.add_hours(
                new Date(temp_upload.date_upload),
                Number(temp_upload.temp));

            if (date_expire <= Date.now()) {
                const storage_path = config.get('pages.storage.path');
                db_upload.delete(temp_upload.id)
                .then(_ => {
                    file_system.delete_file(`${storage_path}/${temp_upload.path}`);
                    file_system.delete_file(`${storage_path}/thumbnails/${temp_upload.path}`);
                });
            }
        });
    })
    .catch(err => {
        logger.error(`remove_expired_temp_uploads: ECHEC ${err}`, 'Upload');
    });
}