// External modules
const mime = require('mime-types');
// Internal modules
const chain_promises = require('../modules/chain_promises');
const db_upload = require('../database/db_upload');
const header_footer_builder = require('../modules/header_footer_builder');
const logger = require('../modules/logger');
const storage = require('./storage');

/**
 * @brief Affiche la page d'accueil du site
 * @param {object} express Instance express (res, req)
 */
module.exports.accueil = express => {
    const promises = [
        { title: 'last_pic_path', promise: db_upload.get_last, args: null },
        { title: 'count_storage', promise: db_upload.request_count, args: '' }
    ];

    chain_promises.chain(promises)
    .then(data => {
        const last_thumb_path = storage.thumbnail_path(data.last_pic_path.path);
        express.res.render('accueil.ejs', {
            data: {
                count_storage: data.count_storage.itemsAvailable,
                last_pic_path: data.last_pic_path.path,
                last_pic_thumbnail_path: last_thumb_path,
                type: mime.contentType(data.last_pic_path.path)
            },
            error: null,
            title: header_footer_builder.title('Accueil'),
            ...express.req.common_data
        });
    })
    .catch(err => {
        logger.error(err, 'Accueil');
        express.res.render('accueil.ejs', {
            data: {
                count_storage: null,
                last_pic_path: null,
                last_pic_thumbnail_path: null
            },
            error: err,
            title: header_footer_builder.title('Accueil'),
            ...express.req.common_data
        });
    });
}