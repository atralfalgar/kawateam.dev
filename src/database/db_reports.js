// Internal modules
const database = require('./database');

/**
 * @brief Ajoute dans la table upload_reports une nouvelle entrée
 * @param {object} data Les données à ajouter en base de donnée
 * @return Une Promise de la transaction à la base de donnée
 */
module.exports.report = async data => {
    return database.transaction(function(trx) {
        return database('upload_reports')
            .transacting(trx)
            .insert(data);
    });
}