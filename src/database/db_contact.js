// Internal modules
const database = require('./database');

/**
 * @brief Ajoute un nouveau message de contact
 * @param {object} data Message, email et ip pour le contact
 * @return Une Promise de la transaction à la base de donnée
*/
module.exports.add = async data => {
    return database.transaction(function(trx) {
        return database('contact')
            .transacting(trx)
            .insert(data);
    });
}