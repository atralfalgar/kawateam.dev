const client_adapter = process.env.SQLITE3_BD_FILE !== undefined ? 'better-sqlite3' : 'pg';

const configuration = {
    client: client_adapter,
    connection: {
        host: 'localhost',
        user: process.env.SERVER_DB_USER,
        password: process.env.SERVER_DB_PWD,
        database: process.env.SERVER_DB_NAME,
        filename: process.env.SQLITE3_BD_FILE
    },
    useNullAsDefault: process.env.SQLITE3_BD_FILE === undefined
};

/**
 * @brief La configuration pour la connexion à la base de donnée.
 * require('knex')(require('database')) pour se connecter
 */
module.exports = require('knex')(configuration);

/**
 * @brief Nom du client utilisé
 */
module.exports.database_client = process.env.SQLITE3_BD_FILE !== undefined ? 'sqlite3' : 'PostgreSQL';