// Internal modules
const database = require('./database');
const datetime = require('../modules/datetime');

/**
 * @brief Ajoute dans la table upload une nouvelle entrée
 * @param {object} data Les données à ajouter en base de donnée
 * @return Une Promise de la transaction à la base de donnée
 */
module.exports.add = async data => {
    return database.transaction(function(trx) {
        return database('upload')
            .transacting(trx)
            .insert(data);
    });
}

/**
 * @brief Retourne les téléversements en fonction de la pagination demandée et de la recherche optionnelle
 * @param {integer} offset Décalage par rapport à zéro pour la pagination
 * @param {integer} limitResult Nombre de résultats à retourner
 * @param {string} sort Méthode de tri
 * @param {string} search (Optionnel) Mot de clé de recherche parmi les tags
 * @returns Une Promise de la transaction à la base de donnée
 */
module.exports.storage = async (offset, limitResult, sort, search = '') => {
    return database('upload')
        .select('id', 'path', 'alt')
        .where('private', 0)
        .andWhere('tags', 'like',  `%${decodeURI(search)}%`)
        .orderBy('date_upload', sort)
        .limit(limitResult)
        .offset(offset);
}

/**
 * @brief Retourne le nombre d'entrées retournée par une requête du storage. Utilisée pour la pagination
 * @param {string} search (Optionnel) Mot de clé de recherche parmi les tags
 * @return Une Promise de la transaction à la base de donnée
 */
module.exports.request_count = (search = '') => {
    return database('upload')
        .count('id', {as: 'itemsAvailable'})
        .where('private', 0)
        .andWhere('tags', 'like', `%${decodeURI(search)}%`)
        .first();
}

/**
 * @brief Retourne les informations sur un téléversement précis
 * @param {integer} id Identidiant du téléversement
 * @returns Une Promise de la transaction à la base de donnée
 */
module.exports.get_by_id = async id => {
    return database('upload')
        .select('id', 'path', 'uploader', 'private', 'tags', 'alt', 'temp', 'date_upload')
        .where('id', id)
        .first();
}

/**
 * @brief Retourne le chemin du dernier téléversement
 * @returns Une Promise de la transaction à la base de donnée
 */
 module.exports.get_last = async _ => {
    return database('upload')
        .select('path')
        .where('private', 0)
        .orderBy('date_upload', 'desc')
        .first();
}

/**
 * @brief Retourne les informations d'un téléversement par son chemin
 * @param {string} path Chemin du téléversement
 * @returns Une Promise de la transaction à la base de donnée
 */
module.exports.get_by_path = async path => {
    return database('upload')
        .select('id', 'tags', 'alt', 'date_upload')
        .where('path', path)
        .first();
}

/**
 * @brief Retourne les téléversements temporaires
 * @returns Une Promise de la transaction à la base de donnée
 */
module.exports.get_temp_uploads = async _ => {
    return database('upload')
        .select('id', 'path', 'temp', 'date_upload')
        .whereNot('temp', 0);
}

/**
 * @brief Retourne les tags associés aux téléchargements publics
 * @returns Une Promise de la transaction à la base de donnée
 */
module.exports.get_public_tags = _ => {
    return database('upload')
        .select('tags')
        .distinct()
        .where('private', 0)
        .where('tags', '!=', '')
        .orderBy('tags', 'asc');
}

/**
 * @brief Retourne le nombre de téléversement dans la dernière heure pour une ip donnée
 * @param {string} ip L'adresse IP
 * @return Une Promise de la transaction à la base de donnée
 */
module.exports.get_upload_count_last_hour_for_ip = async ip => {
    const now_db_format = datetime.get_db_format_now(new Date(), true);
    return database('upload')
        .count('id', { as: 'total' })
        .where('upload.uploader', ip)
        .andWhere('date_upload', '>', now_db_format)
        .first();
}


/**
 * @brief Supprime un téléversement
 * @param {integer} id L'id du téléversement à supprimer
 * @returns Une Promise de la transaction à la base de donnée
 */
module.exports.delete = async id => {
    return database.transaction(function(trx) {
        return database('upload')
            .transacting(trx)
            .where('id', id)
            .del();
    });
}
