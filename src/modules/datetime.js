/**
 * @brief Retourne le nombre de jour dans un mois
 * @param month Le mois dont on souhaite savoir le nombre de jours
 * @param year L'année en question (Pour la gestion de février)
 * @return Le nombre de jour dans le mois
 */
module.exports.get_number_days_in_month = (month, year) => {
    if (month < 0 || month > 11)
        return 0;

    return new Date(year, month, 0).getDate();
}

/**
 * @brief Retourne sous forme YYYY-MM-DD HH:MM:SS la date et heure courante ou la date et heure fournie
 * @param date (Optionnel) Objet date à utiliser (new Date() par défaut)
 * @param minus_two_hours (Optionnel) Retire deux heures à la date retournée (false par défaut)
 * @return Une chaîne de caractère de format YYYY-MM-DD HH:MM:SS. Retpir,e i,e chaîne vide en cas d'erreur
 */
module.exports.get_db_format_now = (date = new Date(), minus_two_hours = false) => {
    if (date == null || typeof date !== 'object')
        return '';

    // Gestion du diff
    let year_diff = 0;
    let month_diff = 0;
    let day_diff = 0;
    let hour_diff = 0;
    if (minus_two_hours && minus_two_hours === true) {
        let hour = date.getHours();
        if (hour - 2 < 0) {
            hour_diff = 22;
            if (date.getDate() == 1) {
                if (date.getMonth() == 0) {
                    month_diff = 11;
                    day_diff = this.get_number_days_in_month(11, date.getFullYear() - 1);
                    year_diff = -1;
                }
                else {
                    day_diff = this.get_number_days_in_month(date.getMonth() - 1, date.getFullYear());
                    month_diff = -1;
                }
            }
            else
                day_diff = -1;
        }
        else {
            hour_diff = -2;
        }
    }

    // Génération de la date
    let month = date.getMonth() + 1 + month_diff;
    if ((date.getMonth() + 1 + month_diff) < 10)
        month = '0' + (date.getMonth() + 1 + month_diff);

    let day = date.getDate() + day_diff;
    if ((date.getDate() + day_diff) < 10)
        day = '0' + (date.getDate() + day_diff);

    let hour = date.getHours() + hour_diff;
    if ((date.getHours() + hour_diff) < 10)
        hour = '0' + (date.getHours() + hour_diff);

    let minute = date.getMinutes();
    if (date.getMinutes() < 10)
        minute = '0' + date.getMinutes();

    let second = date.getSeconds();
    if (date.getSeconds() < 10)
        second = '0' + date.getSeconds();

    return `${date.getFullYear() + year_diff}-${month}-${day} ${hour}:${minute}:${second}`;
}

/**
 * @brief Prends une date venant d'une requête bdd et crée une chaîne de caractères indépendente du SGBD utilisé
 * @param {string} date_str Chaîne de caractère représentant une date
 * @returns Une chaîne de caractère DD/MM/YYYY HH:MM:SS crée à partir de la date fournie
 */
module.exports.display_formatting = date_str => {
    const date = new Date(Date.parse(date_str));
    if (!(date instanceof Date) || isNaN(date))
        return '';

    const hours_localtime = date.getHours();

    const day = date.getDate() >= 10 ? date.getDate() : '0' + date.getDate();
    const month = (date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1);
    const hours = hours_localtime >= 10 ? hours_localtime : '0' + hours_localtime;
    const minutes = date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes();
    const seconds = date.getSeconds() >= 10 ? date.getSeconds() : '0' + date.getSeconds();

    return day + '/' + month+ '/' + date.getFullYear() + ' ' + hours + ':' + minutes + ':' + seconds;
}

/**
 * @brief Ajoute à la date donnée un certain nombre d'heures
 * @param {Date} date La date à utiliser
 * @param {integer} h Le nombre d'heure à ajouter
 * @returns Retourne la date avec le nombre d'heures ajouté. Retourne null en cas d'erreur
 */
module.exports.add_hours = (date, h) => {
    if (!date || !date.getTime || h === null || h === undefined  || typeof h !== 'number')
        return null;
    
    date.setTime(date.getTime() + (h * 3600000));
    return date;
}