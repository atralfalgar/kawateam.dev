// External modules
const config = require('config');

/**
 * @brief Génère le titre de la page à utiliser dans la balise title
 * @param {string} page Titre de la page (optionel)
 * @returns La chaîne de titre à utiliser
 */
module.exports.title = page => {
    const site_title = config.get('server.name');
    const separator = '-';

    // Soit page - NOM_DU_SITE
    // Soit NOM_DU_SITE

    if (page != null && page != '' && typeof page === 'string')
        return `${page} ${separator} ${site_title}`;
    else
        return site_title;
}

/**
 * @brief Indique si il faut ajouter le footer qui indique que c'est une instance de dev
 * @returns Retourne un boolean indiquant si il faut afficher ou non le footer indiquand que c'est une instance de dev
 */
module.exports.footer = _ => {
    return process.env.NODE_ENV !== 'production';
}