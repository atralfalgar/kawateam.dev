// External modules
require('colors');
// Internal modules
const datetime = require('./datetime');

const tag_date = tag => {
    return `${tag} (${datetime.get_db_format_now()})`;
}

const TAGS = {
    TAG_INFO: tag_date('INFO').brightWhite,
    TAG_WARNING: tag_date('WARN').brightYellow,
    TAG_ERROR: tag_date('ERR ').brightRed
};

const build_message = (mod, msg) => {
    if (mod === null || typeof mod !== 'string')
        return msg;
    return `[${mod}] ${msg}`;
}

/**
 * @brief Affiche un message d'information dans la console
 * @param {string} str Le message
 * @param {string} module (Optionnel) Le module générant le message
 */
module.exports.log = (str, mod = null) => {
    if (str !== '' && typeof str === 'string')
        console.log(TAGS.TAG_INFO, build_message(mod, str).white);
}

/**
 * @brief Affiche un message d'avertissement dans la console
 * @param {string} str Le message
 * @param {string} module (Optionnel) Le module générant le message
 */
module.exports.warning = (str, mod = null) => {
    if (str !== '' && typeof str === 'string')
        console.log(TAGS.TAG_WARNING, build_message(mod, str).yellow);
}

/**
 * @brief Affiche un message d'erreur dans la console
 * @param {string} str Le message
 * @param {string} module (Optionnel) Le module générant le message
 */
module.exports.error = (str, mod = null) => {
    if (str !== '' && typeof str === 'string')
        console.log(TAGS.TAG_ERROR, build_message(mod, str).red);
    else {
        console.log(TAGS.TAG_ERROR, build_message(mod, '[Object]:').red);
        this.data(str);
    }
}

/**
 * @brief Affiche en mode information une donnée dans la console
 * @param {*} data Une donnée, peu importe le type, à logger
 */
module.exports.data = data => {
    console.log(TAGS.TAG_INFO, data);
}

/**
 * @brief Affiche un avertissement si une variable est undefined (strict)
 * @param {*} test La variable à tester
 * @param {string} name Le nom de la variable à afficher si elle est undefined
 */
module.exports.warn_undefined = (test, name) => {
    if (test === undefined)
        this.warning(`${name} n'est pas défini`);
}