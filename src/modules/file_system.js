// Node modules
const fs = require('fs');
// External modules
const mime = require('mime-types');
// Internal modules
const logger = require('./logger');

/**
 * @brief Génère une string pour l'affichage à partir d'un nb d'octet
 * @param {integer} octets Un nombre d'octets
 * @returns Une chaîne de caractère avec le nb d'octets. Vide en cas d'erreur
 */
module.exports.humain_display_size = octets => {
    const sizes = [ 'o', 'ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo' ];

    if (octets == null || typeof octets !== 'number' || octets < 0)
        return '';

    let index = 0;
    while (octets >= 1024 && index < sizes.length - 1) {
        index++;
        octets /= 1024;
    }

    return `${Math.round(octets)} ${sizes[index]}`;
}

/**
 * @brief Créé un dossier si il n'existe pas
 * @param {string} directory Le dossier à créer
 * @returns true en cas de succès, false en cas d'échec
 */
module.exports.create_directory_if_not_exists = directory => {
    if (directory == null || typeof directory != 'string' || directory === '')
        return false;
    
    try {
        if (!this.file_exists(directory)) {
            fs.mkdirSync(directory, { recursive: true });
            logger.log(`Création du dossier ${directory}`, 'File_System');
        }
        return true;
    }
    catch (e) {
        logger.error(e);
        return false;
    }
}

/**
 * @brief Supprime le fichier pasé en paramètre et log la suppression
 * @param {string} file Chemin du fichier à supprimer
 * @returns false si le fichier n'existe pas sinon true
 */
module.exports.delete_file = file => {
    if (this.file_exists(file)) {
        fs.unlinkSync(file);
        logger.log(`${file} supprimé`, 'File_System');
        return true;
    }
    else {
        logger.warning(
            `Suppression de ${file} mais le fichier n'existe pas`,
            'File_System');
        return false;
    }
}

/**
 * @brief Teste l'existence d'un fichier
 * @param {string} file Fichier à tester
 * @returns true si le fichier existe sinon false
 */
module.exports.file_exists = file => {
    return fs.existsSync(file);
}

/**
 * @brief Retourne 3 chaînes de caractères avec les extensions acceptés
 * @param {array} accepted_mimes Tableau contenant les mime types autorisés
 * @returns Un objet avec 3 chaînes de caractères (audio, image, video)
 */
module.exports.accepted_types = accepted_mimes => {
    let accepted_ext = { audio: '', image: '', video: '' };

    accepted_mimes.forEach(mime_i => {
        const ext_str = mime.extension(mime_i);
        if (ext_str != false) {
            if (mime_i.includes('audio')) {
                if (accepted_ext.audio === '')
                    accepted_ext.audio += ext_str;
                else
                    accepted_ext.audio += `/${ext_str}`;
            }
            else if (mime_i.includes('image')) {
                if (accepted_ext.image === '')
                    accepted_ext.image += ext_str;
                else
                    accepted_ext.image += `/${ext_str}`;
            }
            else if (mime_i.includes('video')) {
                if (accepted_ext.video === '')
                    accepted_ext.video += ext_str;
                else
                    accepted_ext.video += `/${ext_str}`;
            }
        } else {
            // Cas spécifiques où mime-types n'arrive pas à générer l'extension
            if (mime_i === 'audio/vorbis') {
                if (accepted_ext.audio === '')
                    accepted_ext.audio += 'ogg';
                else
                    accepted_ext.audio += `/ogg`;
            } else if (mime_i === 'image/svg+xml-compressed') {
                // Volontairement ignoré
            } else if (mime_i === 'audio/flac') {
                // Volontairement ignoré
            }
        }
    });

    return accepted_ext;
}

/**
 * @brief Retourne une chaîne avec les MIME acceptés pour le téléversement
 * @param {array} accepted_mimes Tableau contenant les mime types autorisés
 * @returns Une chaîne avec tout les MIME séparés par des virgules
 */
module.exports.accepted_mimes = accepted_mimes => {
    if (!accepted_mimes || accepted_mimes.length < 1)
        return '';

    let str = '';

    accepted_mimes.forEach(mime_i => {
        if (str === '')
            str += mime_i;
        else
            str += `,${mime_i}`;
    });

    return str;
}

/**
 * @brief Retourne la taille du fichier passé en paramètre
 * @param {string} path Chemin du fichier 
 * @returns Le nombre d'octets du fichier. -1 en cas d'erreur
 */
module.exports.get_file_size = path => {
    if (!path || typeof path !== 'string')
        return -1;
    
    try {
        return fs.statSync(path).size;
    }
    catch(e) {
        return -1;
    }
}