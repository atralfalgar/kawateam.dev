// External modules
const ffmpeg = require('fluent-ffmpeg');
const ffmpeg_static = require('ffmpeg-static');
const sharp = require('sharp');
// Internal modules
const file_system = require('./file_system');
const logger = require('./logger');

// Retourne le type générique de fichier à partir du mime
const mime_type = file_mime => {
    if (file_mime.includes('video'))
        return 'video';
    if (file_mime.includes('audio'))
        return 'audio';
    if (file_mime.includes('image'))
        return 'image';

    return 'error';
}

// Ne fais rien car on utilise une vignette statique pour les fichiers audio
const thumbnail_audio = (finalFile, destDir) => {
    return new Promise((resolve) => {
        resolve({
            success: true,
            path: finalFile,
            thumbnail_path: 'thumbnails/' + finalFile
        });
    });
}

// Génère une vignette pour une image et la copie à son dossier de destination
const thumbnail_picture = (finalFile, destDir) => {
    return new Promise((resolve, reject) => {
        sharp(destDir + finalFile)
        .resize(480, 480)
        .toFile(destDir + 'thumbnails/' + finalFile)
        .then(_ => {
            resolve(
            {
                success: true,
                path: finalFile,
                thumbnail_path: 'thumbnails/' + finalFile
            });
        })
        .catch(err => {
            logger.error('Failed to create thumbnail', 'Thumbnail');
            logger.data(err);
            reject(
            {
                success: false,
                error: 'Echec de la création du thumbnail',
                err: err
            });
        });
    });
}

// Génère une vignette pour une vidéo et la copie à son dossier de destination
const thumbnail_video = (finalFile, destDir) => {
    return new Promise((resolve, reject) => {
        try {
            ffmpeg(destDir + finalFile)
            .setFfmpegPath(ffmpeg_static)
            .screenshots({
                count: 1,
                filename: finalFile + '.tmp.png',
                folder: destDir + 'thumbnails/',
                timestamps: [0.0]
            }).on('end', _ => {
                sharp(destDir + 'thumbnails/' + finalFile + '.tmp.png')
                .resize(480, 480)
                .toFile(destDir + 'thumbnails/' + finalFile + '.png')
                .then(_ => {
                    file_system.delete_file(destDir + 'thumbnails/' + finalFile + '.tmp.png');
                    resolve(
                    {
                        success: true,
                        path: finalFile,
                        thumbnail_path: 'thumbnails/' + finalFile + '.png'
                    });
                })
                .catch(err => {
                    logger.error('Failed to create thumbnail', 'Thumbnail');
                    logger.data(err);
                    file_system.delete_file(destDir + 'thumbnails/' + finalFile + '.tmp.png');
                    reject(
                    {
                        success: false,
                        error: 'Echec de la création du thumbnail',
                        err: err
                    });
                });
              });
        }
        catch (error) {
            logger.error('Echec de la création du thumbnail (vidéo)', 'Thumbnail');
            logger.data(error);
            reject(
            {
                error: 'Echec de la création du thumbnail',
                err: error,
                success: false
            });
        }
    });
}

/**
 * @brief Génère une vignette pour une image ou une vidéo et la copie à son dossier de destination
 * @param {string} final_file Chemin vers le fichier temporaire téléversé
 * @param {string} dest_dir Chemin vers le dossier où stocker les fichiers téléversés
 * @param {string} mime Type mime du fichier
 * @returns Une Promise ou null en cas d'erreur
 */
module.exports.generate = (final_file, dest_dir, mime) => {
    const error_promise = _ => new Promise((_, reject) => { reject(); });

    if (!final_file || typeof final_file !== 'string' || !dest_dir || typeof dest_dir !== 'string' || !mime || typeof mime !== 'string')
        return error_promise();

    const generators = {
        'audio' : thumbnail_audio,
        'error' : error_promise,
        'image': thumbnail_picture,
        'video': thumbnail_video
    };

    return generators[mime_type(mime)](final_file, dest_dir);
}