/**
 * @brief Représente un état d'erreur de pagination
 */
module.exports.pagination_error = {
    current: 0,
    total: 0,
    is_first: true,
    is_last: true,
    previous: null,
    next: null,
    root: null
};

/**
 * @brief Calcule le nombre total de page en fonction du nombre d'éléments et du nombre d'éléments par page
 * @param {integer} totalItems Nombre total d'éléments
 * @param {integer} itemsPerPage Nombre d'éléments maximum par page
 * @returns Le nombre total de pages pour paginer tout les éléments. Retourne 0 en cas d'erreur
 */
module.exports.pageCount = (totalItems, itemsPerPage) => {
    if (totalItems == null || itemsPerPage == null || itemsPerPage === 0)
        return 0;

    return (Math.ceil(totalItems / itemsPerPage) > 0 ? Math.ceil(totalItems / itemsPerPage) : 1);
}

/**
 * @brief Génère un objet pagination contenant les informations nécessaire pour pouvoir afficher un outil de pagination
 * @param {string} baseUrl l'URI de base de la page
 * @param {integer} currentPage Numéro de la page actuelle
 * @param {integer} itemTotalNumber Nombre total d'éléments
 * @param {integer} itemsPerPage Nombre d'éléments maximum par page
 * @returns Un objet (current, total, is_first, is_last, previous, next, root). Retourne this.pagination_error en cas d'erreur
 */
module.exports.pagination = (baseUrl, currentPage, itemTotalNumber, itemsPerPage) => {
    if (baseUrl != null && currentPage != null && itemTotalNumber != null && itemsPerPage != null) {
        const availablePages = this.pageCount(itemTotalNumber, itemsPerPage);
        if (availablePages === 0)
            return this.pagination_error;

        const isFirst = currentPage <= 1;
        const isLast = currentPage >= availablePages;

        return {
            current: currentPage,
            total: availablePages,
            is_first: isFirst,
            is_last: isLast,
            previous: isFirst ? null : currentPage - 1,
            next: isLast ? null : currentPage + 1,
            root: `${baseUrl}/page/`
        };
    }
    else {
        return this.pagination_error;
    }
}