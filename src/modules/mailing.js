// External modules
const config = require('config');
const nodemailer = require('nodemailer');
// Internal modules
const logger = require('./logger');

/**
 * @brief Transforme un tableau d'adresses email en une chaîne contenant ces emails séparés d'une virgule
 * @param {Array} array Tableau contenant des adresses email 
 * @returns Une chaîne de caractères contenant les emails séparés d'une virgule
 */
const generate_receiver_string = array => {
    let receiver_string = '';
    array.forEach((email, index) => {
        if (index === 0)
            receiver_string = receiver_string + email;
        else
            receiver_string = receiver_string + ',' + email;
    });
    return receiver_string;
}

/**
 * @brief Envoie un courriel aux administrateurs
 * @param {string} subject Le sujet du message
 * @param {string} msg Le contenu du message
 * @param {boolean} high_priority (Optionel) Indique si le message est de haute priorité ou non (false par défaut)
 * @returns Une Promise. Elle doit être gérée sinon le serveur s'arrêterait en cas d'erreur
 */
module.exports.send_email_admins = (subject, msg, high_priority = false) => {
    return this.send_email(generate_receiver_string(config.get('mail.admins')), subject, msg, high_priority);
}

/**
 * @brief Envoie un courriel
 * @param {string} to Destinataire ou liste des destinataires spéarés par une virgule
 * @param {string} subject Le sujet du message
 * @param {string} msg Le contenu du message
 * @param {boolean} high_priority (Optionel) Indique si le message est de haute priorité ou non (false par défaut)
 * @returns Une Promise. Elle doit être gérée sinon le serveur s'arrêterait en cas d'erreur
 */
module.exports.send_email = (to, subject, msg, high_priority = false) => {
    return new Promise((resolve, reject) => {
        if (config.get('mail.enable') === false) {
            logger.log(`Un mail (sujet : ${subject}) n\' a pas été envoyé car le mailing est désactivé`);
            resolve();
            return;
        }

        if (process.env.MAIL_USERNAME === undefined || process.env.MAIL_PASSWORD === undefined) {
            logger.error('Le username ou le mot de passe pour le serveur mail n\'est pas renseigné', 'Mailing')
            reject('Le username ou le mot de passe pour le serveur mail n\'est pas renseigné');
            return;
        }

        // Options de la connexion et du transport
        const timeout = config.get('mail.timeout') * 1000;
        const options = {
            auth: {
                pass: process.env.MAIL_PASSWORD,
                type: 'login',
                user: process.env.MAIL_USERNAME
            },
            authMethod: 'plain',
            connectionTimeout: timeout,
            debug: process.env.NODE_ENV !== 'production',
            disableFileAccess: true,
            disableUrlAccess: true,
            greetingTimeout: timeout,
            host: config.get('mail.host'),
            port: config.get('mail.port'),
            secure: true,
            socketTimeout: timeout,
            tls: {
                rejectUnauthorized: config.get('mail.strictCertificate')
            }
        };

        // Contenu du courriel
        const mail = {
            encoding: 'utf-8',
            from: config.get('mail.email'),
            html: `<p>${msg}</p>`,
            priority: high_priority ? 'high' : 'normal',
            replyTo: config.get('mail.email'),
            subject: `${config.get('mail.subjectTag')} ${subject}`,
            text: msg,
            to: to
        };

        nodemailer.createTransport(options).sendMail(mail)
        .then(res => {
            // Log des succès et échecs
            if (res.accepted && res.accepted.length > 0) {
                res.accepted.forEach(email => {
                    logger.log(`Courriel envoyé avec succès à ${email}`, 'Mailing');
                });
            }
            if (res.rejected && res.rejected.length > 0) {
                res.rejected.forEach(email => {
                    logger.log(`Courriel non envoyé à ${email}`, 'Mailing');
                });
            }

            resolve();
        })
        .catch(error => {
            logger.error(`${error}`, 'Mailing');
            reject(error);
        });
    });
}