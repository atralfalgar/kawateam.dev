// Internal modules
const logger = require('./logger');

/**
 * @brief Enchaîne les appels aux Promises fournies et retourne un objet avec les valeurs retournées par ces Promises
 * @param {array} promises_data Tableau contenant les Promises à enchaîner. Chaque membre du tableau doit contenir title (string), promise (function) et args (object, optionel)
 * @returns Une Promise qui contiendra l'aggrégation des données retournées par les différentes Promises fournies
 */
module.exports.chain = promises_data => {
    return new Promise(async (resolve, reject) => {
        try {
            if (promises_data == null || promises_data.length == null) {
                reject('Erreur : promises_data est null, undefined ou n\'est pas un tableau', 'Chain_promises');
                return;
            }
            
            let data = { };

            for (let i = 0; i < promises_data.length; i++) {
                if (promises_data[i].promise == null || promises_data[i].title == null)
                    continue;
                
                const promise_data = await promises_data[i].promise(promises_data[i].args);
                Object.assign(data, { [promises_data[i].title]: promise_data });
            }

            resolve(data);
        }
        catch (err) {
            logger.error(err);
            reject(err);
        }
    });
}