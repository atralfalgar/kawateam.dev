// External modules
const compression = require('compression');
const config = require('config');
const express = require('express');
const fileUpload = require('express-fileupload');
const helmet = require("helmet");
const morgan = require('morgan');
const serve_favicon = require('serve-favicon');
// Internal modules
const database = require('./database/database');
const file_system = require('./modules/file_system');
const logger = require('./modules/logger');
const page_404 = require('./controllers/404');
const page_data = require('./middleware/page_data');
const router = require('./routers/router');
const router_maintenance = require('./routers/router_maintenance');
const tasks = require('./modules/tasks');
const upload = require('./controllers/upload');

/**
 * @brief L'instance express du serveur
 */
module.exports.server = express();

/**
 * @brief Callback appelée au lancement du serveur
 */
module.exports.onStart_callback = null;

/**
 * @brief Démarre le serveur
 * @param {function} onStart Fonction appelée lorsque le serveur est lancé.
 *                  Un paramètre est passé : le SGBD utilisé par l'instance
 */
module.exports.start = onStart => {
    // Initialization
    if (!file_system.create_directory_if_not_exists(config.get('pages.storage.path') + '/thumbnails')) {
        onStart(null);
        return ;
    }

    this.onStart_callback = onStart;
    
    // Express middlewares
    if (process.env.SERVER_TEST === undefined)
        this.server.use(morgan('HTTP (:date[iso]) :method\t:status\t:remote-addr\t:response-time ms\t:url'));
    if (process.env.NODE_ENV === 'production')
        this.server.use(helmet());

    const maintenance = config.get('server.maintenance');

    this.server.use(compression())
    .set('trust proxy', 1)
    .use(express.urlencoded({ extended: false }))
    .use(express.json())
    .use(fileUpload({ limits: { fileSize: upload.MAX_UPLOAD_SIZE} }))
    .use(config.get('server.base_uri'), express.static(__dirname + '/../public'))
    .use(serve_favicon(__dirname + '/../public/img/favicon.ico'))

    // Middleware erreurs
    .use((err, req, res, next) => {
        logger.error(err.stack, 'Middleware');
        res.status(500).send('Erreur Middleware');
    })
    
    // Site router
    .use('/', maintenance ? router_maintenance : router)

    // Unhandled routes
    .use(page_data, (req, res) => {
        page_404.page_404({ req: req, res: res });
    });

    // Start
    this.server.listen(config.get('server.port'), this.post_start);
}

module.exports.post_start = _ => {
    if (process.env.SERVER_TEST !== undefined)
        return;
    
    // Création des tâches cron du serveur
    tasks.new_minute_task(upload.remove_expired_temp_uploads).start();

    // Callback
    this.onStart_callback(database.database_client);
}

/**
 * @brief Ferme le serveur express, stoppe les tâches lancées et stoppe
 * @param {object} server L'instance express
 * @param {boolean} test Instance pour les tests unitaires
 */
module.exports.cleanup = (server, test = false) => {
    logger.log('Server clean up');
    tasks.cleanup();
    if (server)
        server.close();
    database.destroy();
    logger.log('Server cleaned up');
    if (!test)
        process.exit(0);
}