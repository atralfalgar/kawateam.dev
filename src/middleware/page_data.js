// External modules
const config = require('config');
// Internal modules
const header_footer_builder = require('../modules/header_footer_builder');

/**
 * @brief Middleware pour injecter les données communes à toutes les pages
 * @param {object} req Requete express
 * @param {object} res Réponse express
 * @param {function} next Requete express
 */
module.exports = (req, res, next) => {
    req.common_data = {
        config: config,
        dev: header_footer_builder.footer(),
        site_version: require('../../package.json').version
    }
    next();
}