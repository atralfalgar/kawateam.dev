const get_file_from_path = str => {
    return str.split(/\\/).pop().split(/\//).pop();
}
const updateLabel = input => {
    if (input && input.srcElement && input.srcElement.value)
        document.getElementById('inputFileText').textContent = get_file_from_path(input.srcElement.value);
}

document.getElementById('fileinput').addEventListener('change', updateLabel);