/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('upload', table => {
            table.increments('id').primary();
            table.string('path').notNullable().unique();
            table.string('uploader').notNullable();
            table.integer('private').notNullable().defaultTo(0);
            table.string('tags').notNullable();
            table.string('alt').notNullable();
            table.integer('temp').notNullable().defaultTo(0);
            table.datetime('date_upload').notNullable().defaultTo(knex.fn.now());
        })
        .createTable('upload_reports', table => {
            table.increments('id').primary();
            table.integer('picture_id').unsigned().notNullable();
            table.datetime('date').notNullable().defaultTo(knex.fn.now());
            table.string('reporter_ip').notNullable();

            table.foreign('picture_id').references('upload.id').onUpdate('CASCADE').onDelete('CASCADE');
        })
        .createTable('contact', table => {
            table.increments('id').primary();
            table.string('email').defaultTo(null);
            table.string('message').notNullable();
            table.string('ip').notNullable();
            table.datetime('date').notNullable().defaultTo(knex.fn.now());
        });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
        .dropTableIfExists('upload')
        .dropTableIfExists('upload_reports')
        .dropTableIfExists('contact');
};
