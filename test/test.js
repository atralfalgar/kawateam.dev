// External modules
const chai = require('chai');
const chaiHttp = require('chai-http');
// Internal modules
const server = require('../src/server');

// Configure chai
chai.use(chaiHttp);
chai.should();

let expressInstance = null;

// Initialisation du serveur
before(function() { expressInstance = server.start(_ => {}); });

// Nettoyage post-tests
after(function() { server.cleanup(expressInstance, true); });