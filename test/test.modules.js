// External modules
const assert = require('chai').assert;
const config = require('config');
// Node modules
const fs = require('fs');
// Internal modules
const chain_promises = require('../src/modules/chain_promises');
const datetime = require('../src/modules/datetime');
const db_upload = require('../src/database/db_upload');
const file_system = require('../src/modules/file_system');
const header_footer_builder = require('../src/modules/header_footer_builder');
const pagination = require('../src/modules/pagination');
const tasks = require('../src/modules/tasks');
const thumbnail = require('../src/modules/thumbnail');

// Tests des modules du projet
describe('Modules', function() {
    describe('chain_promises', function() {
        it('Fonction chain', function() {
            const promises = [
                { title: 'storage_api0', promise: db_upload.get_last, args: null },
                { title: 'storage_api1', promise: db_upload.get_public_tags, args: null },
                { title: 'storage_api2', promise: db_upload.get_last, args: null },
                { title: 'storage_api3', promise: db_upload.get_public_tags, args: null },
                { title: 'storage_api4', promise: db_upload.get_last, args: null },
                { title: 'storage_api5', promise: db_upload.get_public_tags, args: null },
                { title: 'storage_api6', promise: db_upload.get_last, args: null },
                { title: 'storage_api7', promise: db_upload.get_public_tags, args: null }
            ];
        
            return chain_promises.chain(promises);
        });
        it('Gestion erreurs 1', function(done) {
            chain_promises.chain(null)
            .then(function() {
                assert.fail();
            })
            .catch(function() {
                done();
            });
        });
        it('Gestion erreurs 2', function(done) {
            const promises = [
                { },
                { }
            ];

            chain_promises.chain(promises)
            .then(function() {
                assert.fail();
            })
            .catch(function() {
                done();
            });
        });
        it('Gestion erreurs 3', function(done) {
            chain_promises.chain('Not an array')
            .then(function() {
                assert.fail();
            })
            .catch(function() {
                done();
            });
        });
    });
    describe('datetime', function() {
        it('Fonction get_number_days_in_month', function() {
            assert.typeOf(datetime.get_number_days_in_month(1, 2021), 'number');
            assert.equal(datetime.get_number_days_in_month(1, 2021), 31);
            assert.equal(datetime.get_number_days_in_month(2, 2021), 28);
            assert.equal(datetime.get_number_days_in_month(4, 2021), 30);
        });
        it('Fonction get_db_format_now', function() {
            assert.typeOf(datetime.get_db_format_now(), 'string');
            assert.typeOf(datetime.get_db_format_now(new Date()), 'string');
            assert.typeOf(datetime.get_db_format_now(new Date(), true), 'string');
            assert.equal(datetime.get_db_format_now(new Date('December 17, 1995 03:24:00')), '1995-12-17 03:24:00');
            assert.equal(datetime.get_db_format_now(new Date('December 17, 1995 03:24:00'), true), '1995-12-17 01:24:00');
            assert.equal(datetime.get_db_format_now(new Date('July 5, 2021 14:04:02')), '2021-07-05 14:04:02');
            assert.equal(datetime.get_db_format_now(new Date('July 5, 2021 14:04:02'), true), '2021-07-05 12:04:02');
            assert.equal(datetime.get_db_format_now(new Date('July 5, 2021 01:04:02'), true), '2021-07-04 23:04:02');
            assert.equal(datetime.get_db_format_now(new Date('July 5, 2021 00:04:02'), true), '2021-07-04 22:04:02');
        });
        it('Fonction display_formatting', function() {
            assert.typeOf(datetime.display_formatting(new Date()), 'string');
            assert.typeOf(datetime.display_formatting(new Date('July 5, 2021 14:04:02')), 'string');
            assert.equal(datetime.display_formatting(new Date('July 5, 2021 14:04:02')), `05/07/2021 14:04:02`);
            assert.equal(datetime.display_formatting(new Date('December 17, 1995 03:24:00')), `17/12/1995 03:24:00`);
        });
        it('Fonction add_hours', function() {
            assert.deepEqual(datetime.add_hours(new Date(2022, 1, 29, 15, 0, 0), 0), new Date(2022, 1, 29, 15, 0, 0));
            assert.deepEqual(datetime.add_hours(new Date(2022, 1, 29, 15, 0, 0), 1), new Date(2022, 1, 29, 16, 0, 0));
            assert.deepEqual(datetime.add_hours(new Date(2022, 1, 29, 15, 0, 0), 2), new Date(2022, 1, 29, 17, 0, 0));
            assert.deepEqual(datetime.add_hours(new Date(2022, 1, 29, 15, 0, 0), 9), new Date(2022, 1, 30, 0, 0, 0));
        });
        it('Gestion erreurs', function() {
            assert.equal(datetime.get_number_days_in_month(-1, 2021), 0);
            assert.equal(datetime.get_number_days_in_month(13, 2021), 0);
            assert.equal(datetime.get_number_days_in_month(2, 0), 28);
            assert.equal(datetime.get_db_format_now(null, true), '');
            assert.equal(datetime.get_db_format_now(1234, true), '');
            assert.typeOf(datetime.display_formatting('Not a date'), 'string');
            assert.equal(datetime.display_formatting('Not a date'), '');
            assert.typeOf(datetime.display_formatting('December 35, 1995 03:24:00'), 'string');
            assert.equal(datetime.display_formatting('December 35, 1995 03:24:00'), '');
            assert.typeOf(datetime.display_formatting('December 3, 1995 26:24:00'), 'string');
            assert.equal(datetime.display_formatting('December 3, 1995 26:24:00'), '');
            assert.isNull(datetime.add_hours());
            assert.isNull(datetime.add_hours(new Date()));
            assert.isNull(datetime.add_hours(1, 1));
            assert.isNull(datetime.add_hours(new Date(), 'Not a valid value'));
        });
    });
    describe('file_system', function() {
        it('Fonction humain_display_size', function() {
            assert.typeOf(file_system.humain_display_size(), 'string');
            assert.equal(file_system.humain_display_size(), '');
            assert.equal(file_system.humain_display_size('Not a number'), '');
            assert.equal(file_system.humain_display_size({ value: 0 }), '');
            assert.equal(file_system.humain_display_size(-50), '');
            assert.typeOf(file_system.humain_display_size(0), 'string');
            assert.equal(file_system.humain_display_size(0), '0 o');
            assert.equal(file_system.humain_display_size(1024), '1 ko');
            assert.equal(file_system.humain_display_size(1048576), '1 Mo');
            assert.equal(file_system.humain_display_size(1073741824), '1 Go');
            assert.equal(file_system.humain_display_size(1099511627776), '1 To');
            assert.equal(file_system.humain_display_size(1125899906842624), '1 Po');
            assert.equal(file_system.humain_display_size(1152921504606846976), '1 Eo');
            assert.equal(file_system.humain_display_size(1180591620717411303424), '1 Zo');
            assert.equal(file_system.humain_display_size(1208925819614629174706176), '1 Yo');
            assert.equal(file_system.humain_display_size(1208925819614629174706176 * 1024), '1024 Yo');
            assert.equal(file_system.humain_display_size(10485760), '10 Mo');
            assert.equal(file_system.humain_display_size(107374182400), '100 Go');
            assert.equal(file_system.humain_display_size(1024.1), '1 ko');
        });
        it('Fonction create_directory_if_not_exists', function() {
            const directory_test = __dirname + '/test_test/';

            assert.isFalse(file_system.create_directory_if_not_exists());
            assert.isFalse(file_system.create_directory_if_not_exists(null));
            assert.isFalse(file_system.create_directory_if_not_exists(2));
            assert.isFalse(file_system.create_directory_if_not_exists({}));

            assert.isFalse(fs.existsSync(directory_test));
            assert.isTrue(file_system.create_directory_if_not_exists(directory_test));
            assert.isTrue(fs.existsSync(directory_test));
            fs.rmdirSync(directory_test);
        });
        it('Fonction delete_file', function() {
            assert.isFalse(file_system.delete_file());
            assert.isFalse(file_system.delete_file(42));
            assert.isFalse(file_system.delete_file('/tmp/notExistentFile'));

            const file_test_path = __dirname + '/test.test';
            fs.closeSync(fs.openSync(file_test_path, 'w'));
            assert.isTrue(file_system.delete_file(file_test_path));
        });
        it('Fonction file_exists', function() {
            assert.isFalse(file_system.file_exists());
            assert.isFalse(file_system.file_exists(''));
            assert.isFalse(file_system.file_exists('/tmp/notExistentFile'));
            assert.isTrue(file_system.file_exists(__dirname + '/test.js'));
        });
        it('Fonction accepted_types', function() {
            const mime_test = [ 'audio/vorbis', 'audio/midi', 'image/gif', 'image/png', 'image/svg+xml-compressed', 'video/mp4' ];
            assert.typeOf(file_system.accepted_types([]), 'object');
            assert.typeOf(file_system.accepted_types(mime_test), 'object');
            assert.typeOf(file_system.accepted_types(mime_test).audio, 'string');
            assert.typeOf(file_system.accepted_types(mime_test).image, 'string');
            assert.typeOf(file_system.accepted_types(mime_test).video, 'string');
            assert.deepEqual(file_system.accepted_types(mime_test), { audio: 'ogg/mid', image: 'gif/png', video: 'mp4' });
        });
        it('Fonction accepted_mimes', function() {
            const mime_test = [ 'audio/midi', 'image/gif', 'video/mp4' ];
            assert.typeOf(file_system.accepted_mimes([]), 'string');
            assert.typeOf(file_system.accepted_mimes(mime_test), 'string');
            assert.equal(file_system.accepted_mimes(mime_test), 'audio/midi,image/gif,video/mp4');
        });
        it('Fonction get_file_size', function() {
            assert.typeOf(file_system.get_file_size(__dirname + '/test.js'), 'number');
            assert.isAbove(file_system.get_file_size(__dirname + '/test.js'), 10);
            assert.equal(file_system.get_file_size(__dirname + '/non_existant_file.js'), -1);
            assert.equal(file_system.get_file_size(), -1);
        });
    });
    describe('header_footer_builder', function() {
        it('Fonction title', function() {
            assert.typeOf(header_footer_builder.title('test'), 'string');
            assert.equal(header_footer_builder.title('test'), `test - ${config.get('server.name')}`);
            assert.equal(header_footer_builder.title(), config.get('server.name'));
        });
        it('Fonction footer', function() {
            assert.typeOf(header_footer_builder.footer(), 'boolean');
            assert.equal(header_footer_builder.footer(), (process.env.NODE_ENV !== 'production'));
        });
        it('Gestion erreurs', function() {
            assert.equal(header_footer_builder.title(1234), config.get('server.name'));
        });
    });
    describe('pagination', function() {
        it('Fonction pageCount', function() {
            assert.typeOf(pagination.pageCount(100, 10), 'number');
            assert.equal(pagination.pageCount(100, 10), 10);
            assert.equal(pagination.pageCount(1, 10), 1);
            assert.equal(pagination.pageCount(0, 10), 1);
            assert.equal(pagination.pageCount(11, 10), 2);
        });
        it('Fonction pagination', function() {
            assert.typeOf(pagination.pagination('/test', 1, 10, 5), 'object');
            assert.typeOf(pagination.pagination('/test', 1, 10, 5).current, 'number');
            assert.typeOf(pagination.pagination('/test', 1, 10, 5).total, 'number');
            assert.typeOf(pagination.pagination('/test', 1, 10, 5).is_first, 'boolean');
            assert.typeOf(pagination.pagination('/test', 1, 10, 5).is_last, 'boolean');
            assert.typeOf(pagination.pagination('/test', 1, 10, 5).previous, 'null');
            assert.typeOf(pagination.pagination('/test', 25, 100, 18).previous, 'number');
            assert.typeOf(pagination.pagination('/test', 1, 10, 5).next, 'number');
            assert.typeOf(pagination.pagination('/test', 25, 100, 18).next, 'null');
            assert.typeOf(pagination.pagination('/test', 1, 10, 5).root, 'string');
            assert.deepEqual(pagination.pagination('/test', 1, 10, 5), 
                {
                    current: 1,
                    total: 2,
                    is_first: true,
                    is_last: false,
                    previous: null,
                    next: 2,
                    root: '/test/page/'
            });
            assert.deepEqual(pagination.pagination('/test', 25, 100, 18), 
                {
                    current: 25,
                    total: 6,
                    is_first: false,
                    is_last: true,
                    previous: 24,
                    next: null,
                    root: '/test/page/'
            });
            assert.deepEqual(pagination.pagination('/test', 25, 10000, 99), 
                {
                    current: 25,
                    total: 102,
                    is_first: false,
                    is_last: false,
                    previous: 24,
                    next: 26,
                    root: '/test/page/'
            });
        });
        it('Gestion erreurs', function() {
            assert.typeOf(pagination.pageCount(11, 0), 'number');
            assert.equal(pagination.pageCount(11, 0), 0);
            assert.equal(pagination.pageCount(0, 0), 0);
            assert.typeOf(pagination.pagination(), 'object');
            assert.equal(pagination.pagination(), pagination.pagination_error);
            assert.equal(pagination.pagination(null, null, null, null), pagination.pagination_error);
            assert.equal(pagination.pagination('/test', 1, 0, 0), pagination.pagination_error);
            assert.equal(pagination.pagination(), pagination.pagination_error);
        });
    });
    describe('tasks', function() {
        it('Tableau tasks', function() {
            assert.deepEqual(tasks.tasks, []);
            assert.lengthOf(tasks.tasks, 0);
        });
        it('Fonction remove_task', function() {
            const test_task = tasks.new_minute_task(_ => {});
            const test_task2 = tasks.new_minute_task(_ => {});

            assert.lengthOf(tasks.tasks, 2);
            tasks.remove_task(test_task);
            assert.lengthOf(tasks.tasks, 1);
            tasks.remove_task(test_task2);
            assert.lengthOf(tasks.tasks, 0);
        });
        it('Fonction new_minute_task', function() {
            const test_task = tasks.new_minute_task(_ => {});
            assert.typeOf(test_task, 'object');
            assert.lengthOf(tasks.tasks, 1);
            tasks.remove_task(test_task);
        });
        it('Fonction new_hour_task', function() {
            const test_task = tasks.new_hour_task(_ => {});
            assert.typeOf(test_task, 'object');
            assert.lengthOf(tasks.tasks, 1);
            tasks.remove_task(test_task);
        });
        it('Fonction new_custom_task', function() {
            const test_task = tasks.new_custom_task('*/2 * * * *', _ => {});
            assert.typeOf(test_task, 'object');
            assert.lengthOf(tasks.tasks, 1);
            tasks.remove_task(test_task);
        });
        it('Fonction cleanup', function() {
            assert.lengthOf(tasks.tasks, 0);
            for (let i = 0 ; i < 20 ; i++) {
                const test_task = tasks.new_minute_task(_ => {});
                test_task.start();
            }
            assert.lengthOf(tasks.tasks, 20);
            tasks.cleanup();
            assert.lengthOf(tasks.tasks, 0);
        });
        it('Gestion erreurs', function() {
            let test_task = tasks.new_minute_task();
            assert.isNull(test_task);
            assert.lengthOf(tasks.tasks, 0);

            test_task = tasks.new_hour_task();
            assert.isNull(test_task);
            assert.lengthOf(tasks.tasks, 0);

            test_task = tasks.new_minute_task(_ => {});
            assert.lengthOf(tasks.tasks, 1);
            tasks.remove_task();
            tasks.remove_task(null);
            assert.lengthOf(tasks.tasks, 1);
            tasks.remove_task(test_task);

            test_task = tasks.new_custom_task();
            assert.isNull(test_task);
            test_task = tasks.new_custom_task('* * * * *');
            assert.isNull(test_task);
            test_task = tasks.new_custom_task('* * * * *', null);
            assert.isNull(test_task);
            test_task = tasks.new_custom_task(null, _ => {});
            assert.isNull(test_task);
            test_task = tasks.new_custom_task(42, _ => {});
            assert.isNull(test_task);
            test_task = tasks.new_custom_task('Nonsense string', _ => {});
            assert.isNull(test_task);
        });
    });
    describe('thumbnail', function() {
        const test_thumbnail_dest = __dirname + '/icon.test.jpg';
        after(function() {
            if (fs.existsSync(test_thumbnail_dest))
                fs.unlinkSync(test_thumbnail_dest);
        });
        it('Fonction generate', function() {
            // Test non fonctionnel pour le moment
            /*if (os.platform() !== 'win32') // Ne fonctionne pas sous Windows
                return thumbnail.generate(__dirname + '/../public/img/icon.jpg', test_thumbnail_dest, 'image/jpeg');*/
        });
        describe('Gestion erreurs', function() {
            it('generate pas d\'argument', function() {
                thumbnail.generate()
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
            it('generate 1 seul argument', function() {
                thumbnail.generate('')
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
            it('generate 2 mauvais arguments', function() {
                thumbnail.generate('', '')
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
            it('generate 3 mauvais arguments 1', function() {
                thumbnail.generate('', '', 42)
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
            it('generate 2 mauvais arguments 2', function() {
                thumbnail.generate(42, 42, 42)
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
        });
    });
});