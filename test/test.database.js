// External modules
const assert = require('chai').assert;
// Internal modules
const db_reports = require('../src/database/db_reports');
const db_upload = require('../src/database/db_upload');

// Tests des API
describe('API', function() {
    describe('db_reports', function() {
        describe('Gestion erreurs', function() {
            it('report', function() {
                db_reports.report()
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
        });
    });
    describe('db_upload', function() {
        it ('storage', function() {
            return db_upload.storage(0, 10, 'desc');
        });
        it ('storage (avec recherche)', function() {
            return db_upload.storage(0, 10, 'desc', 'test');
        });
        it ('request_count', function() {
            return db_upload.request_count();
        });
        it ('request_count (avec recherche)', function() {
            return db_upload.request_count('test');
        });
        it ('get_public_tags', function() {
            return db_upload.get_public_tags();
        });
        it ('get_last', function() {
            return db_upload.get_last();
        });
        it ('get_upload_count_last_hour_for_ip', function() {
            return db_upload.get_upload_count_last_hour_for_ip('127.0.0.1');
        });
        it ('get_temp_uploads', function() {
            return db_upload.get_temp_uploads();
        });
        it ('#126 Tags avec accents', async function() {
            const ids = await db_upload.add({
                path: 'test',
                uploader: 'test.database.js',
                private: false,
                tags: 'éé',
                alt: 'éé',
                temp: false });
            const res = await db_upload.request_count('éé');
            await db_upload.delete(ids[0]);
            assert.isAbove(res.itemsAvailable, 0);
        });
        describe('Gestion erreurs', function() {
            it('get_upload_count_last_hour_for_ip Pas d\'arguments', function() {
                db_upload.get_upload_count_last_hour_for_ip()
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
            it('storage Pas d\'arguments', function() {
                db_upload.storage()
                .then(function() {
                    assert.fail();
                })
                .catch(function() {
                    done();
                });
            });
        });
    });
});