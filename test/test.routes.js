// Node modules
const fs = require('fs');
// External modules
const assert = require('chai').assert;
const chai = require('chai');
const config = require('config');
// Internal modules
const server = require('../src/server');

// Tests de différentes routes du serveur
describe('Routes', function() {
    describe('/', function() {
        it('GET /', function(done) {
            this.slow(100);
            chai.request(server.server)
                .get('/')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    assert.include(res.res.text, config.get('server.name'));
                    chai.expect(res.text).to.match(/Statistiques/);
                    assert.include(res.res.text, require('../package.json').version);
                    done();
                 });
        });
        it('GET /404', function(done) {
            chai.request(server.server)
                .get('/404')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(404);

                    chai.expect(res.text).to.match(/Page non trouvée/);
                    done();
                 });
        });
        it('GET /nonexistent', function(done) {
            chai.request(server.server)
                .get('/nonexistent')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(404);

                    chai.expect(res.text).to.match(/Page non trouvée/);
                    done();
                 });
        });
        it('GET /teapot', function(done) {
            chai.request(server.server)
                .get('/teapot')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(418);
                    done();
                 });
        });
    });
    describe('/upload', function() {
        const textes_communs = [
            /Téléversement/,
            /Téléverser un fichier/,
            /Téléversement privé/,
            /Téléversement temporaire/
        ];
        it('GET /upload', function(done) {
            chai.request(server.server)
                .get('/upload')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
        it('GET /upload/413', function(done) {
            chai.request(server.server)
                .get('/upload/413')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
        it('POST /upload (No file)', function(done) {
            chai.request(server.server)
                .post('/upload')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
        it('POST /upload (Wrong mime file)', function(done) {
            chai.request(server.server)
                .post('/upload')
                .attach('picFile', fs.readFileSync( __dirname + '/test.js'), 'test.js')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
    });
    describe('/storage', function() {
        const textes_communs = [
            /Stockage/
        ];
        it('GET /storage', function(done) {
            chai.request(server.server)
                .get('/storage')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
        it('GET /storage/page/1', function(done) {
            chai.request(server.server)
                .get('/storage/page/1')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
        it('GET /storage/test', function(done) {
            chai.request(server.server)
                .get('/storage/test')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
        it('GET /storage/test/page/1', function(done) {
            chai.request(server.server)
                .get('/storage/test/page/1')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);

                    textes_communs.forEach(regex => {
                        chai.expect(res.text).to.match(regex);
                    });
                    done();
                 });
        });
    });
    describe('/view', function() {
        it('GET /view', function(done) {
            chai.request(server.server)
                .get('/view')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);
                    done();
                 });
        });
    });
    describe('/contact', function() {
        it('GET /contact', function(done) {
            chai.request(server.server)
                .get('/contact')
                .end(function(err, res) {
                    assert.isNull(err);
                    res.should.have.status(200);
                    done();
                 });
        });
    });
});