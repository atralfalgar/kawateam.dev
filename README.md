# kawateam.dev

Service de téléversement et stockage d'images, sons et vidéos pour le site *[kawateam.dev](https://kawateam.dev)*.

## Auteur(ice-s)

* Bruno Visse <bruno.visse.gamedev@gmail.com>

## Contributeur(trice-s)

* Ellena Lubert <pacmiam@tuxfamily.org>

## Dépendances

* NodeJS >=18
* Serveur PostgreSQL en localhost (pour la production)

Les dépendances node sont listées dans *package.json* et sont installées et gérées par npm.

## Licence

Le projet est sous licence *[GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)*.

## Mise en place

### Environnement

Le serveur requiert que les variables d'environnement suivantes soient définies :

 - *SECRET_SESSION*
 - *SERVER_DB_NAME*
 - *SERVER_DB_PWD*
 - *SERVER_DB_USER*

Pour l'accès aux ressources statiques (téléversements), un reverse proxy doit être mis en place, ou alors il sera nécessaire de désactiver le téléversement et la page stockage. Vous avez la possibilité de déterminer où les téléversements seront stockées sur le serveur ainsi que l'URI du stockage. Vous devez modifier le fichier de configuration du serveur pour refléter la configuration du reverse proxy.

### Configuration

Le fichier de configuration de l'instance est situé dans le dossier /config. Vous y trouverez le fichier *default.json* contenant une configuration standard pour le développement.

Pour une instance de production, créez dans le même dossier le fichier *production.json*. Chaque option précisée dans ce fichier remplacera les valeurs par défaut si la variable d'environnement *NODE_ENV* est définie avec la valeur *production*.

Voici la liste des options disponibles :
* mail.admins (*boarrayolean*) : tableau contenant les emails des administrateurs
* mail.enable (*boolean*) : active l'envoi de courriels
* mail.email (*string*) : adresse email d'envoi de courriel
* mail.host (*string*) : hôte du serveur mail
* mail.port (*integer*) : port du serveur mail
* mail.timeout (*integer*) : nombre de secondes du timeout pour l'envoi de courriels
* mail.strictCertificate (*string*) : indique si il faut vérifier si le serveur mail utilise bien un certificat généré par une autorité de certification
* mail.subjectTag (*string*) : chaîne de caractère ajoutée en début du sujet de chaque mail envoyé
* pages.contact.enable (*boolean*) : active la fausse page d'administration
* pages.contact.base_uri (*string*) : URI de base de la page de contact
* pages.storage.base_uri (*string*) : URI de base de la page du stockage
* pages.storage.enable (*boolean*) : active la page du stockage
* pages.storage.path (*string*) : chemin vers le dossier de stockage
* pages.storage.pagination_size (*integer*) : nombre d'éléments affichés sur la page des téléversements
* pages.storage.sort (*string*) : "desc" ou "asc" seulement. Ordre d'affichage sur la page des téléversements
* pages.storage.static_uri (*string*) : URI utilisée pour les téléversements statiques. Doit correspondre à la configuration du reverse proxy
* pages.upload.accepted_mime_types (*array*) : liste des types de fichier (mime type) acceptés par le module d'upload
* pages.upload.base_uri (*string*) : URI de base de la page du téléversement
* pages.upload.enable (*boolean*) : active la page du téléversement
* pages.upload.max_upload_ip (*integer*) : indique la limite de téléversement autorisé pour une ip dans la dernière heure. 0 désactive la limitation
* pages.upload.max_upload_size_mo (*integer*) : taille maximum acceptée en Mo en téléversement
* pages.upload.upload_allowed (*boolean*) : active le téléversement
* pages.view.base_uri (*string*) : URI de base de la page de vue de téléversement
* pages.view.title (*string*) : titre de la page de vue de téléversement
* server.base_uri (*string*) : URI de base du site. Vide si il n'y en a pas
* server.maintenance (*boolean*) : active le mode maintenance du site
* server.name (*string*) : nom/titre du site
* server.port (*integer*) : port d'écoute du serveur

### Envoi de courriels

Pour permettre l'envoi de courriel par le site, vous devez disposer d'un serveur mail opérationel. Le site utilisera ce serveur mail pour l'envoi de courriels.

Vous devez tout d'abord indiquer dans le fichier de configuration les informations nécessaires pour que le site puisse utiliser votre serveur mail. Ensuite, il va falloir mettre en place plusieurs variables d'environnement pour l'authentification à votre serveur mail :
 - *MAIL_USERNAME*
 - *MAIL_PASSWORD*

Cette configuration peut être ignorée si vous désactivez l'envoi de courriel dans le fichier de configuration.

### Installation des dépendances
```
npm install
npm install --omit=dev (sans les dépendences de dev)
```

## Utilisation

### Lancement du serveur pour la production

Complétez le fichier *knexfile.exemple.js* avec les informations de connexion à la base de donnée afin que la migration de la base de donnée puisse être faite. Sauvegardez ensuite ce fichier sous le nom *knexfile.js*. Le serveur PostgreSQL en localhost soit être opérationel avant de lancer ces commandes.

```
npm run migrate:latest
npm start --production
```

### Lancer les tests

Le lancement des tests requiert que la base de données sqlite3 soit mise en place. Si seuls les modules de production sont installés, les tests ne sont pas disponibles.
```
npm test
```

### Lancer le serveur avec sqlite3

Notez que le support sqlite3 n'est pas destiné à être utilisé en production.

Pour utiliser une base de données locale sqlite3, créez une variable d'environnement *SQLITE3_BD_FILE* contenant le chemin vers le fichier de base de données.

### Lancer le serveur pour le développement

Pour mettre en place une base de données sqlite3, vous pouvez lancer ces commandes (uniquement pour GNU/Linux) :
```
npm run dev:migrate:latest
npm run dev:sqlite-init
```

La commande suivante lance localement le serveur en utilisant le fichier *dev.db* créé avec les commandes précédentes :
```
npm run dev
```

*Note* : En utilisant sqlite3, les dates affichées seront potentiellement en décalage par rapport à l'heure locale.

Vous pouvez modifier les variables d'environnement pour le dévelopement en éditant le fichier *dev.env*.