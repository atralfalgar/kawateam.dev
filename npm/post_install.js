// Node modules
const fs = require('fs');

const FORK_AWESOME_PATH = './node_modules/fork-awesome';
const CSS_FOLDER = 'public/css';
const FONTS_FOLDER = 'public/fonts';

// Installation de Fork Awesome
if (fs.existsSync(FORK_AWESOME_PATH)) {
    if (!fs.existsSync(CSS_FOLDER))
        fs.mkdirSync(CSS_FOLDER, { recursive: true });

    fs.copyFileSync(`${FORK_AWESOME_PATH}/css/fork-awesome.min.css`, `${CSS_FOLDER}/fork-awesome.min.css`);
    if (!fs.existsSync(FONTS_FOLDER))
        fs.mkdirSync(FONTS_FOLDER, { recursive: true });
    fs.cpSync(`${FORK_AWESOME_PATH}/fonts/`, FONTS_FOLDER, { recursive: true });
}