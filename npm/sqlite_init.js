const dotenv = require('dotenv');

dotenv.config({ path: 'dev.env' });

const data_test_insert = async _ => {
    try {
        const database = require('../src/database/database');

        await database.transaction(function(trx) {
            return database('upload')
                .transacting(trx)
                .insert([
                    {
                        path: 'picture.jpeg',
                        uploader: '192.168.0.1',
                        private: 0,
                        tags: 'picture thing',
                        alt: 'picture thing'
                    },
                    {
                        path: 'picture hidden.jpeg',
                        uploader: '192.168.0.1',
                        private: 1,
                        tags: 'hidden',
                        alt: ''
                    },
                    {
                        path: 'picture2.jpeg',
                        uploader: '192.168.0.1',
                        private: 0,
                        tags: '',
                        alt: ''
                    },
                    {
                        path: 'picture3.jpeg',
                        uploader: '192.168.0.1',
                        private: 0,
                        tags: 'picture',
                        alt: ''
                    },
                    {
                        path: 'picture4.jpeg',
                        uploader: '192.168.0.1',
                        private: 0,
                        tags: 'thing',
                        alt: ''
                    }
                ]);
        });

        await database.transaction(function(trx) {
            return database('upload_reports')
                .transacting(trx)
                .insert([
                    {
                        picture_id: 1,
                        reporter_ip: '22.55.22.55'
                    }
                ]);
        });

        await database.transaction(function(trx) {
            return database('contact')
                .transacting(trx)
                .insert([
                    {
                        email: 'test@test.test',
                        message: 'Test Test',
                        ip: '22.55.22.55'
                    }
                ]);
        });
    }
    catch(e) {
        console.error('Sqlite-init', e);
    }
}

data_test_insert()
.then(_ => {
    process.exit(0);
})
.catch(_ => {
    process.exit(1);
});