const dotenv = require('dotenv');
const nodemon = require('nodemon');

dotenv.config({ path: 'dev.env' });

nodemon({
    script: 'src/main.js',
    config: 'config/nodemon.json'
});

nodemon.on('quit', _ => {
    console.log('====================');
    console.log('Fin du serveur');
    process.exit();
})
.on('restart', files => {
    console.log('====================');
    if (files) {
        console.log('Changements détéctés :');
        files.forEach(file => {  
            console.log(`\t> ${file}`);
        });
    }
    console.log('Redémarrage du serveur');
    console.log('====================');
})
.on('crash', _ => {
    console.log('====================');
    console.log('Crash du serveur détécté');
});