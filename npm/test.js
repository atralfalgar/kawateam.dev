const dotenv = require('dotenv');
const child_process = require('child_process');

dotenv.config({ path: 'test.env' });

try {
    const output = child_process.execSync('mocha -c --check-leaks --bail --exit --trace-deprecation', { encoding: 'utf-8' });
    console.log(output);
}
catch (e) {
    console.log(e.stdout);
}