// Update with your config settings and save it as knexfile.js

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {
    development: {
      client: 'better-sqlite3',
      connection: {
        filename: './dev.db'
      },
      migrations: {
        tableName: 'knex_migrations'
      }
    },
  
    production: {
      client: 'pg',
  
      connection: {
        /* Définir/remplir ces champs sur le serveur de production */
        host: 'localhost',
        user: process.env.SERVER_DB_USER,
        password: process.env.SERVER_DB_PWD,
        database: process.env.SERVER_DB_NAME,
        filename: process.env.SQLITE3_BD_FILE
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: {
        tableName: 'knex_migrations'
      }
    }
  };
  